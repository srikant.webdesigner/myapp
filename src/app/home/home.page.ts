import { Component } from '@angular/core';
import { YoutubeService } from '../youtube.service';
import { Video } from './video';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

@Component({

  selector: 'app-home',
  templateUrl: 'home.page.html',
  providers: [ YoutubeService ],
  styleUrls: ['home.page.scss'],

})

export class HomePage {

  dangerousVideoUrl: string;
  videoUrl: SafeResourceUrl;
  
  medivideo:Video;


  constructor(private youtubeservice: YoutubeService, private sanitizer: DomSanitizer ) {



  }

  ngOnInit() {

    this.showConfig();
  }

  showConfig() {

    this.youtubeservice.getJson().subscribe(response =>{
      this.medivideo={ ... response.body };
      console.log(this.medivideo);
    });

  }

  public getSantizeUrl(value: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(value);
}

}

