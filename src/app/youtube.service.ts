import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import {Video} from './home/video';


@Injectable({

  providedIn: 'root'

})

export class YoutubeService {
  

  configUrl = 'http://mediyoga.in/videos.json';



  constructor(private http: HttpClient) { }

  getJson(): Observable<HttpResponse<Video>> {

     return this.http.get<Video>(this.configUrl, { observe: 'response' }
     );
    }

}

